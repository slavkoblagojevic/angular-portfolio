// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBIA9XL-mL4seShXoUgCJw3DNWDi2U3GNw",
    authDomain: "slavkoblagojevic-1914c.firebaseapp.com",
    projectId: "slavkoblagojevic-1914c",
    storageBucket: "slavkoblagojevic-1914c.appspot.com",
    messagingSenderId: "1034677399881",
    appId: "1:1034677399881:web:ac067fb04315b7b6272a98"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
