export const environment = {
    service: {
        languages: [
          {
              id: 'rs',
              tag: 'ap_srpski',
              flag: 'ср',
              date_format: 'dd.MM.yyyy.',
              weather_tag: "sr"
          },
          {
              id: 'en',
              tag: 'ap_english',
              flag: 'en',
              date_format: 'MMM.dd,yyyy',
              weather_tag: "en"
          }
        ],

    },
    apiKey: "9bb333a78b3fe59b5b99914a5d4a7f30",
    defaultLang: "en",
}