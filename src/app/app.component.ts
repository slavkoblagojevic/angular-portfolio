import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from './services/language.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {

  currentRoute: string | undefined;
  
  constructor(
    private _languageService: LanguageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    const langFromLocalStorage: string = this._languageService.getLanguageFromLocalStorage() ?? 'en';
    this._languageService.initLanguage(langFromLocalStorage);

    this.router.events.subscribe( event => {
      if (event instanceof NavigationEnd) {
        this.currentRoute = event.url;
        console.log(this.currentRoute);
      }
    })
  }
}
