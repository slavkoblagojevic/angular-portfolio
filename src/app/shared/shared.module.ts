import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LanguageSelectorComponent } from '../components/language-selector/language-selector.component';
import { SwiperModule } from 'swiper/angular';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from '../components/header/header.component';
import { CustomButtonComponent } from '../components/custom-button/custom-button.component';
import { FooterComponent } from '../components/footer/footer.component';



@NgModule({
  declarations: [
    LanguageSelectorComponent,
    HeaderComponent,
    CustomButtonComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    SwiperModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  exports: [
    //MODULES
    SwiperModule,
    CommonModule,
    RouterModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule,
    //COMPONENTS
    LanguageSelectorComponent,
    HeaderComponent,
    CustomButtonComponent,
    FooterComponent
  ]
})
export class SharedModule { }
