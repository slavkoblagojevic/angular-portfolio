import { Component, Input, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { ILanguage, LanguageIdTypes } from 'src/app/models/translate.model';
import { LanguageService } from 'src/app/services/language.service';

@Component({
  selector: 'app-language-selector',
  templateUrl: './language-selector.component.html',
  styleUrls: ['./language-selector.component.scss'],
})
export class LanguageSelectorComponent  implements OnInit {

  //based on input change language selector style
  @Input() whiteColor: boolean = false;
  @Input() gray: boolean = true;
  @Input() primaryColorIcons: boolean = true;
  @Input() intro: boolean = false;
  @Input() header: boolean = false;
  @Input() outline: boolean = false;

  languages: ILanguage[];
  public selectedLanguage: any;

  constructor(
    private _languageService: LanguageService,
    private _translate: TranslateService
  ) {
    this.languages = this._languageService.getLanguages();
    this.selectedLanguage = this.languages?.find((element) => element.id === this._translate.currentLang)?.id;

    this._translate.onLangChange
      .subscribe((event: LangChangeEvent) => {
        this.selectedLanguage = event.lang as LanguageIdTypes;
      });
  }

  ngOnInit() {
    console.log()
  }

  /* ----- */

  public setLanguage(): void {
    this._languageService.setLanguage(this.selectedLanguage);
  }


}
