import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom-button',
  templateUrl: './custom-button.component.html',
  styleUrls: ['./custom-button.component.scss'],
})
export class CustomButtonComponent  implements OnInit {

  @Input() name: string = '';
  @Input() path: string = '';
  @Input() background: boolean = false;

  constructor() { }

  ngOnInit() {}

}
