import { Component, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { IWeatherResponseObject } from 'src/app/models/wether.model';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.page.html',
  styleUrls: ['./weather.page.scss'],
})
export class WeatherPage implements OnInit {

    private _unsubscribeAll: Subject<void> = new Subject<void>();
    weatherData?: IWeatherResponseObject;
    weatherForecastData: any; //TODO interface
    airQualityData: any; //delete
    weatherIconBaseUrl: string = 'https://openweathermap.org/img/wn/';
    
    constructor(
        private _wetherService: WeatherService
    ) {
        this._wetherService.getWetherData();
        this._wetherService.weatherData$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe( (data: IWeatherResponseObject) => {
                if (data){
                    this.weatherData = data;
                }
            });

        this._wetherService.getDailyForecast();
        this._wetherService.weatherForecastData$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe( (data: any) => {
                if (data){
                    console.log(data);
                    this.weatherForecastData = data;
                }
            });
            
    }

    ngOnInit() {
    }

    /* ----- */

    formatHour(hour: any) {}
    airQualityIndexes(){}

    public getWindSpeedKmPerHour(windSpeed: number): number {

        const kmPerHour = windSpeed * 3.6;
        const roundedSpeed = Math.round(kmPerHour);

        return roundedSpeed;
    }

    convertDateFormat(timestamp: number): string {
        const date = new Date(timestamp * 1000);
        const hours = date.getHours().toString().padStart(2, '0');
        const minutes = date.getMinutes().toString().padStart(2, '0');
        return `${hours}:${minutes}`;
    }
}
