import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  public segment: string = 'skills';

  constructor(public navCtrl: NavController) { }

  ngOnInit() {
    console.log();
  }

}
