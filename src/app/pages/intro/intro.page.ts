import { Component, OnInit, ViewChild } from '@angular/core';
import { IntroService } from 'src/app/services/intro.service';
import { SwiperComponent } from 'swiper/angular';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  @ViewChild('introSwiper', {static: false}) swiper?: SwiperComponent;

  showArrows: boolean = true;
  sliderReachEnd: boolean = false;
  activeIndex: number = 0;

  constructor(
    private _introService: IntroService
  ) { }

  ngOnInit() {
    console.log()
  }

  /* ----- */

  public slideChanged(event: any): void {
    console.log(event);
    this.activeIndex = event[0].activeIndex;
    console.log(this.activeIndex);
  }
  
  public keyPressed(e:any,a:any): void { }

  public async skipAll(): Promise<void> {
    this._introService.skipAllAndGoToHomePage();
  }
  
  public slideNext(): void {
    this.swiper?.swiperRef.slideNext(100);
  }

  public slidePrev(): void{
    this.swiper?.swiperRef.slidePrev(100);
  }
}
