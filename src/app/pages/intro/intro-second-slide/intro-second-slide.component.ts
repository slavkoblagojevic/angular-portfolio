import { Component, OnInit } from '@angular/core';
import { IntroService } from 'src/app/services/intro.service';

@Component({
  selector: 'app-intro-second-slide',
  templateUrl: './intro-second-slide.component.html',
  styleUrls: ['./intro-second-slide.component.scss'],
})
export class IntroSecondSlideComponent  implements OnInit {

  constructor(
    private _introService: IntroService
  ) { }

  ngOnInit() {
    console.log();
  }
  /* ----- */

  public getStarted(): void {
    this._introService.skipAllAndGoToHomePage();
  }

}
