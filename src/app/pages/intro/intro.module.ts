import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { IntroPageRoutingModule } from './intro-routing.module';

import { IntroPage } from './intro.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { IntroFirstSlideComponent } from './intro-first-slide/intro-first-slide.component';
import { IntroSecondSlideComponent } from './intro-second-slide/intro-second-slide.component';
import { IntroUpperPartComponent } from './intro-upper-part/intro-upper-part.component';

@NgModule({
  imports: [
    SharedModule,
    IonicModule,
    IntroPageRoutingModule
  ],
  declarations: [
    IntroPage,
    IntroFirstSlideComponent,
    IntroSecondSlideComponent,
    IntroUpperPartComponent
  ]
})
export class IntroPageModule {}
