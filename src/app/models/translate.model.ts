export type LanguageIdTypes = 'rs' | 'la' | 'en';
export type LanguageFlagTypes = 'cp' | 'sr' | 'en';

export interface ILanguage {
    id: string,
    tag: string,
    flag: string,
    date_format: string
}