import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class IntroGuard implements CanActivate {
  
  constructor(
    private _router: Router
  ) {}
  
  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {

    const currentLocation = this._router.getCurrentNavigation()?.finalUrl?.toString();
    const introViewed = localStorage.getItem('intro');
    console.log('current: ', currentLocation);
    console.log('intro :', introViewed);
 
    if (currentLocation !== '/intro') {
      //for example: home
      if (introViewed !== 'ok') {
        this._router.navigate(['/intro']);
        return false;
      }
      return true;
    } else {
      //intro
      if (introViewed !== 'ok') {
        return true;
      } else {
        this._router.navigate(['/home']);
        return false;
      }
    }

  }
  
}