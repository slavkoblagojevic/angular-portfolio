import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, Subject, takeUntil } from 'rxjs';
import { environment } from 'src/environments/environment.ap';

@Injectable({
  providedIn: 'root'
})
export class WeatherService implements OnDestroy {

  private _unsubscribeAll: Subject<void> = new Subject<void>();
  weatherData$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  weatherForecastData$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  baseUrl: string = "https://api.openweathermap.org/data/2.5/weather";
  baseUrlForecast: string = "https://api.openweathermap.org/data/2.5/forecast";
  apiKey: string = environment.apiKey;
  currentWeatherLang: string = this.getWeatherTag(environment.defaultLang);

  constructor(
    private _http: HttpClient,
    private _translateService: TranslateService
  ) {
    this.currentWeatherLang = this.getWeatherTag(this._translateService.currentLang);
    this._translateService.onLangChange
      .pipe(takeUntil(this._unsubscribeAll))
    this._translateService.onLangChange
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((langEvent: LangChangeEvent) => {
        console.log(langEvent);
        this.currentWeatherLang = this.getWeatherTag(langEvent.lang);
        this.getWetherData();
      });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  /* ----- */

  public getWetherData(): void {
    let url = this.baseUrl;
    let latNs = "45.2539";
    let lonNs = "19.8250";
    let params = new HttpParams();
    params = params.append("lat", latNs);
    params = params.append("lon", lonNs);
    params = params.append("lang", this.currentWeatherLang);
    params = params.append("appid", this.apiKey);
    params = params.append("units", "metric");

    let options = {
      params: params
    }

    this._http.get(url, options)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe( (data: any) => {
        if (data) {
          this.weatherData$.next(data);
        } else {
          console.log('Api weather forecast error');
        }
    });

  }

  public getWeatherTag(lang: string): string {
    return environment.service.languages.find(
      (langObj) => lang === langObj.id
    )?.weather_tag || environment.defaultLang;
  }

  public getDailyForecast(): void {
    let url = this.baseUrlForecast;
    let latNs = "45.2539";
    let lonNs = "19.8250";

    let params = new HttpParams();
    params = params.append("lat", latNs);
    params = params.append("lon", lonNs);
    params = params.append("lang", this.currentWeatherLang);
    params = params.append("appid", this.apiKey);
    params = params.append("units", "metric");

    let options = {
      params: params
    }

    this._http.get(url, options)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe( (data: any) => {
        if (data) {
          this.weatherForecastData$.next(data);
        } else {
          console.log('Api weather forecast error');
        }
    });
  }
}
