import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment.ap';
import { ILanguage } from '../models/translate.model';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  private applicationLanguage: string | null = null;
  private languages: ILanguage[] = environment.service.languages;
  private languageTags = this.languages.map(language => language.id);

  constructor(
    private _translate: TranslateService
  ) {}

  public initLanguage(langFromLocalStorage: string): void {
    
    this.applicationLanguage = langFromLocalStorage;
    if (!this.applicationLanguage) {
      this.applicationLanguage = 'en';
    }

    this._translate.addLangs(this.languageTags);
    this._translate.setDefaultLang(this.applicationLanguage);
    this._translate.use(this.applicationLanguage);
    localStorage.setItem('lang', this.applicationLanguage);
  }

  public getLanguageFromLocalStorage(): string | null {
    return localStorage.getItem('lang');
  }

  public getLanguages(): ILanguage[] {
    return this.languages;
  }

  public getLanguageTags(): string[] {
    return this.languageTags;
  }

  public setLanguage(language: string): void {
    this.applicationLanguage = language;
    this._translate.use(this.applicationLanguage);
    localStorage.setItem('lang', this.applicationLanguage);
  }

}
