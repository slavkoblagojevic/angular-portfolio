import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class IntroService {

  constructor(
    private _router: Router
  ) { }

  public skipAllAndGoToHomePage() {
    localStorage.setItem('intro', 'ok');
    this._router.navigate(['/home']);
  }
}
